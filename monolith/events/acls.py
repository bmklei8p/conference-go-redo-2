import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_image(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    try:
        res = requests.get(url, headers=headers)
    except IndexError:
        return None
    return res.json()["photos"][0]["src"]["original"]


def get_coord(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"
    res = requests.get(url)
    try:
        result = {"lat": res.json()[0]["lat"], "lon": res.json()[0]["lon"]}
    except IndexError:
        return None
    return result


def get_weather(lat, lon):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    res = requests.get(url)
    try:
        weather = {
            "temp": res.json()["main"]["temp"],
            "description": res.json()["weather"][0]["description"],
        }
    except IndexError:
        return None
    return weather
