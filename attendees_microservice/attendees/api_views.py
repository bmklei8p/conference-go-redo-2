from django.http import JsonResponse
from common.modelEncoder import ModelEncoder
from .models import Attendee, ConferenceVO
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.http import require_http_methods
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created"]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        try:
            badge = o.badge.__str__()
        except ObjectDoesNotExist:
            badge = "No badge"
        return {"conference": o.conference.name, "badge": badge}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeListEncoder, safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference ID"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            {"attendee": attendee}, encoder=AttendeeDetailEncoder, safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=pk)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid conference ID"},
                status=400,
            )
    else:
        count, _ = Attendee.objects.get(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
